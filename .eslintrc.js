module.exports = {
  extends: ['airbnb-base', 'plugin:prettier/recommended'],
  parser: 'babel-eslint',
  env: {
    es6: true,
    node: true
  },
  rules: {
    'import/prefer-default-export': 0
  }
};
